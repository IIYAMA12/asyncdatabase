# Asyncdatabase

## Description
Asynchronous database table controller for MTA. The data is based on key-value pairs and supports multiple values, including tables converted to JSON format.

### Supports
- SQL
- MySQL
- Multi tables: To prevent SQL injection attacks, avoid using user input directly as table names in MySQL queries. Instead, use predetermined and hardcoded table names to reduce the risk of unauthorized commands or query manipulation.

### Cache
A cache system is implemented to enhance request speed. By default, cache items can exist for 10 to 20 minutes.

### Important notes!
- Tablenames in the database are suffixed with "__async".
- When saving tables, all keys of the table and sub-tables that are strings with numeric values (e.g., "123" or "-10") will be converted to numbers when loading the data. Due to JSON limitations, some table arrays cannot be converted to JSON arrays. To address this, any string keys that can be converted to numbers will now be converted to numbers.

## Installation
1. Rename the file `.env[please-remove-to-enable]` to `.env`.
2. Modify the `.env` file and fill in the required information.
3. Start the resource.
4. If not already done, go in-game.
5. Check the debug console (`/debugscript 3`) to verify the database connection. You can also check the server console.

## Usage
For examples and syntax, refer to the `examples.lua` file.

## Repo
https://gitlab.com/IIYAMA12/asyncdatabase