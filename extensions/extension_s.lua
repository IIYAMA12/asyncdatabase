-- >>> "local storageResource" is concatenate here with extension-loader_s.lua
local callbacks = {}

-- http://lua-users.org/wiki/CopyTable
local function deepcopy( orig )
	local orig_type = type( orig )
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do copy[deepcopy( orig_key )] = deepcopy( orig_value ) end
		setmetatable( copy, deepcopy( getmetatable( orig ) ) )
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end

local cleanTableForJSON
do
	local validTypes = { number = true, string = true, boolean = true }
	function cleanTableForJSON( theTable )
		if type( theTable ) ~= 'table' then return end
		for i = 1, #theTable do
			local value = theTable[i]
			local valType = type( value )
			if valType == 'table' then
				cleanTableForJSON( value )
			elseif not validTypes[valType] then -- nil, userdata/pointers
				theTable[i] = false
			end
		end
	end
end

--[[
	Fixing string key mismatch
]]
local function fixingTableJsonConversion( theTable )
	if type( theTable ) ~= 'table' then return end

	-- Collect all keys before fixing
	local keys = {}
	for key, value in pairs( theTable ) do keys[#keys + 1] = key end

	for i = 1, #keys do
		local key = keys[i]
		local value = theTable[key]
		local valType = type( value )
		if valType == 'table' then
			fixingTableJsonConversion( value )
		elseif type( key ) == "string" and tonumber( key ) then
			theTable[tonumber( key )] = value -- set to a number
			theTable[key] = nil -- remove string variant
		end
	end
end

local function saveData( tableName, key, value )
	if type( value ) == "table" then
		value = deepcopy( value ) -- deepcopy before cleaning
		cleanTableForJSON( value )
	end
	local status, responseMessage = call( storageResource, "saveData", tableName, key, value )
	return status, responseMessage
end

local function loadData( tableName, key, callback )
	if type( callback ) ~= "function" then
		local responseMessage = "Unable to load value. No callback function provided."
		outputDebugString( responseMessage, 2 )
		return false, responseMessage
	end
	local status, response = call( storageResource, "loadData", tableName, key )
	if response then
		local responseType = response.type
		local responseValue = response.value
		if responseType == "CALLBACK" then
			if not responseValue then return false, "ERROR: No responseValue" end
			callbacks[responseValue] = { func = callback, time = getTickCount() }
			return true
		elseif responseType == "CACHE" then
			fixingTableJsonConversion( responseValue )
			callback( responseValue )
			return true
		elseif responseType == "ERROR" then
			local responseMessage = "ERROR:" .. responseValue
			outputDebugString( responseMessage, 1 )
			callback( nil, responseMessage )
			return false, responseMessage
		end
	end
	return false, "ERROR: No response"
end

local function removeData( tableName, key )
	return call( storageResource, "removeData", tableName, key ) -- 
end

local function runQuery( ... )
	local arguments = { ... }
	--[[
		Check if a callback function is provided
	]]
	local callback = arguments[1]
	local callbackAvailable = type( callback ) == "function"
	if callbackAvailable then table.remove( arguments, 1 ) end
	local response = call( storageResource, "runQuery", callbackAvailable, unpack( arguments ) )
	if response then
		local responseType = response.type
		local responseValue = response.value
		if responseType == "CALLBACK" then if callbackAvailable then callbacks[responseValue] = { func = callback, time = getTickCount() } end end
		if responseType == "ERROR" then
			callback( nil, "ERROR" )
			return false, "ERROR"
		end
		return true
	end
	return false, "ERROR: No response"
end

--[[
	Handle the response
	-- Also allow duplicate extensions.
]]
if not callbackGroups then callbackGroups = {} end
callbackGroups[storageResource] = callbacks

--[[
	Call-back to the correct resource
]]
if not responseStorageData then
	function responseStorageData( callbackID, value )
		local callbacks = callbackGroups[sourceResource]
		if callbacks and callbacks[callbackID] then
			local callback = callbacks[callbackID].func
			callbacks[callbackID] = nil
			fixingTableJsonConversion( value )
			callback( value )
			return true
		end
		return false
	end
end

--[[
	Clean up old callbacks when the storage resource has stopped
]]
addEventHandler( "onResourceStop", root, function( stoppedResource )
	if storageResource ~= stoppedResource then return end
	--[[
		Collect old callbacks
	]]
	local oldCallBacks = {}
	for callback in pairs( callbacks ) do oldCallBacks[#oldCallBacks + 1] = callback end
	--[[
		Remove old callbacks from the callbacks table after a delay
	]]
	setTimer( function()
		for i = 1, #oldCallBacks do
			local callback = oldCallBacks[i]
			if callbacks[callback] then callbacks[callback] = nil end
		end
	end, 1000, 1 )
end, true, "low" )

local extensionMethods = { save = saveData, load = loadData, remove = removeData, query = runQuery }

--[[
	* returns <controller> or nil otherwise.
]]
local function tableInit( tableName )
	if not call( storageResource, "tableInit", tableName ) then return end
	return {
		init = tableInit, --
		tableName = tableName, --
		save = function( self, ... )
			if type( self ) ~= "table" then error( "Expected syntax <controller>:save(<key>, <value>). You might have used . instead of :", 2 ) end
			return saveData( self.tableName, ... )
		end, -- 
		load = function( self, ... )
			if type( self ) ~= "table" then error( "Expected syntax <controller>:load(<key>, <function>). You might have used . instead of :", 2 ) end
			return loadData( self.tableName, ... )
		end, --
		remove = function( self, ... )
			if type( self ) ~= "table" then error( "Expected syntax <controller>:remove(<key>). You might have used . instead of :", 2 ) end
			return removeData( self.tableName, ... )
		end, --
		query = function( self, ... )
			if type( self ) ~= "table" then error( "Expected syntax <controller>:query(<query>, ...). You might have used . instead of :", 2 ) end
			return runQuery( ... )
		end --
	}
end
extensionMethods.init = tableInit

return extensionMethods
