addEventHandler( "onResourceStart", resourceRoot, function()
	--[[
		Get the database extension
	]]
	database = loadstring( call( getResourceFromName( "asyncdatabase" ), "getExtension" ) )()
	--[[
		Supports multiple database extensions:
		database1 = loadstring( call( getResourceFromName( "storage1" ), "getExtension" ) )()
		database2 = loadstring( call( getResourceFromName( "storage2" ), "getExtension" ) )()
	]]

	--[[
		You must init a table before you can use it. This ensures that you do not create accidently more tables than needed. 
		It is very easy to make typo's in things like this. You might consider using a variable.
	]]
	database.init( "tableName" )

	--[[
        What can be stored?
		- Strings
		- Tables
		- Numbers
		- Booleans
        - Nil values... :/
	]]
	database.save( "tableName", "key 1", "save me" )
	database.save( "tableName", "key 2", {} )
	database.save( "tableName", "key 3", 123 )
	database.save( "tableName", "key 4", true )
	database.save( "tableName", "key 5", nil )

	--[[
		Load data
	]]
	database.load( "tableName", "key 1", function( value ) iprint( "response", value ) end )

	--[[
		Load multi results
	]]
	database.load( "tableName", { "key 1", "key 2", "key 3" }, function( value ) iprint( "response", value["key 1"], value["key 2"], value["key 3"] ) end )

	--[[
		Remove data
	]]
	database.remove( "tableName", "key 2" )

	--[[
		Use custom queries with or without callbacks
        Note: Without callbacks you will not be able to return values
		Warning: Tables managed by the methods above are making use of a memory cache, modifying the data here causes desync within the cache.
	]]
	database.query( function( result ) iprint( result ) end, "SELECT * FROM tableName__async" )
	database.query( "SELECT * FROM tableName__async" ) -- < not so useful query...
end )

--[[
	Flavour number 2... (with some sugar of course)
	Using a table controller from the init.
]]
addEventHandler( "onResourceStart", resourceRoot, function()
	--[[
		Get the database extension
	]]
	database = loadstring( call( getResourceFromName( "asyncdatabase" ), "getExtension" ) )()

	--[[
		Init the table and return the <tableController>
		On the table controller you can use the same methods and you do not have to rewrite the table name every single time.
	]]
	local tableController = database.init( "tableName" )

	--[[
		Save data
	]]
	tableController:save( "key 1", "save me" )
	tableController:save( "key 2", {} )
	tableController:save( "key 3", 123 )
	tableController:save( "key 4", true )
	tableController:save( "key 5", nil )

	--[[
		Load data
	]]
	tableController:load( "key 1", function( value ) iprint( "response", value ) end )

	--[[
		Load multi results
	]]
	tableController:load( { "key 1", "key 2", "key 3" }, function( value ) iprint( "response", value["key 1"], value["key 2"], value["key 3"] ) end )

	--[[
		Remove data
	]]
	tableController:remove( "key 2" )

	--[[
		Use custom queries with or without callbacks
        Note: Without callbacks you will not be able to return values
		Warning: Tables managed by the methods above are making use of a memory cache, modifying the data here causes desync within the cache.
	]]
	tableController:query( function( result ) iprint( result ) end, "SELECT * FROM tableName__async" )
	tableController:query( "SELECT * FROM tableName__async" ) -- < not so useful query...
end )
