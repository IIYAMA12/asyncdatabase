# Automatic Tester
This resource is used to automatic test the asyncdatabase resource, making sure everything stays healthy.

The file test_s.lua contains the test. It will randomly execute reads, writes, resource restarts.