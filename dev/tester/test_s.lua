--[[
	This resource is used to automatic test the asyncdatabase resource.
]] --
local tester = Tester:new()

addEventHandler( "onResourceStart", resourceRoot, function()
	local hasRestartResourcePermission = hasObjectPermissionTo( getThisResource(), "function.restartResource", true )
	if not hasRestartResourcePermission then
		local warnMsg1 = "Please grant this resource permission to restart another resource. This is will help with testing the memory cache."
		local warnMsg2 = "Quick fix: /aclrequest allow thisResourceName all"
		outputChatBox( warnMsg1 )
		outputDebugString( warnMsg1, 2 )
		outputChatBox( warnMsg2 )
		outputDebugString( warnMsg2, 2 )
	end

	--[[
		Get the database extension
	]]
	database = loadstring( call( getResourceFromName( "asyncdatabase" ), "getExtension" ) )()
	local tableName = "testtable"
	local tableController = database.init( tableName )
	tableController:query( "DELETE FROM testtable__async" )
	restartResource( getResourceFromName( "asyncdatabase" ) )

	-- Doing initial testing
	do
		local keyLength = 3
		local key1 = randomString( keyLength )
		local key2 = randomString( keyLength )
		local key3 = randomString( keyLength )
		local key4 = randomString( keyLength )
		local value = randomString( keyLength )

		tester:iprint( "START Init test" )
		local responsesRequired = 4
		-- Save
		tester:assert( tableController:save( key1, value ) )
		tester:assert( database.save( tableName, key1, value ) )

		-- Load
		tester:assert( tableController:load( key1, function() responsesRequired = responsesRequired - 1 end ) )
		tester:assert( database.load( tableName, key1, function() responsesRequired = responsesRequired - 1 end ) )

		-- Load multiple
		tester:assert( tableController:save( key2, value ) )
		tester:assert( tableController:save( key3, value ) )
		tester:assert( tableController:save( key4, value ) )
		tester:assert( tableController:load( { key1, key2, key3, key4 }, function() responsesRequired = responsesRequired - 1 end ) )
		tester:assert( database.load( tableName, { key1, key2, key3, key4 }, function() responsesRequired = responsesRequired - 1 end ) )

		-- Remove
		tester:assert( tableController:remove( key1 ) )
		tester:assert( database.remove( tableName, key1 ) )
		tester:assert( database.remove( tableName, key2 ) )
		tester:assert( tableController:remove( key2 ) )

		-- Query
		tableController:query( "SELECT * FROM testtable__async LIMIT 1" )
		database.query( "SELECT * FROM testtable__async LIMIT 1" )

		tableController:query( "DELETE FROM testtable__async" )
		tester:iprint( "END Init test" )
		setTimer( function() tester:assert( responsesRequired == 0, "Something went wrong with the initial db responses" ) end, 500, 1 )
	end

	tester:addPreCall( function()
		-- Add new jobs
		tester:addJob( {
			func = function( tester )
				local randomTask = (math.random( 1000 ) % 10) + 1
				local keyLength = math.random( 3 )
				if randomTask == 1 then
					local key = randomString( keyLength )
					tester:iprint( "Save", key )
					local value = randomString( keyLength )
					-- Randomly use a method
					tableController:save( key, value )

					tester:assert( tester:setMemory( key, value ) )

					local request = createTestRequest()
					tableController:query( function( result )
						request.success = true
						tester:assert( result[1] and result[1].count == 1, "Expected 1 item in the database, got:", result[1] and result[1].count )
					end, "SELECT COUNT(*) AS count FROM testtable__async WHERE ID = ?", key )
				elseif randomTask == 2 then
					local key = randomString( keyLength )

					local memoryValue = tester:getMemory( key )
					local request = createTestRequest()
					tester:iprint( "Load", key )
					tester:assert( tableController:load( key, function( value )
						request.success = true

						if type( value ) == "table" then
							if toJSON( memoryValue ) ~= toJSON( value ) then
								tester:reportMemory( key, value, memoryValue, tester:getMemory( key ), toJSON( memoryValues[key] ) )
							end
						elseif memoryValue ~= value then
							tester:reportMemory( key, value, memoryValue, tester:getMemory( key ) )
						end
					end ) )

				elseif randomTask == 3 then
					local key = randomString( keyLength )
					tester:iprint( "Remove", key )
					tester:setMemory( key, nil )
					tester:assert( tableController:remove( key ) )
				elseif randomTask == 4 then
					tester:iprint( "Load multi" )
					local keys = {}
					local keyCount = math.random( 700 )
					local memoryValues = {}
					for i = 1, keyCount do
						keys[i] = randomString( keyLength )
						memoryValues[keys[i]] = tester:getMemory( keys[i] )
					end
					tester:assert( tableController:load( keys, function( returnValue )
						if not returnValue then tester:error( "failed to load multi" ) end
						for i = 1, keyCount do
							local key = keys[i]
							if type( returnValue[key] ) == "table" then
								if toJSON( returnValue[key] ) ~= toJSON( memoryValues[key] ) then
									tester:reportMemory( key, returnValue[key], memoryValues[key], tester:getMemory( key ), toJSON( memoryValues[key] ) ) --
								end
							elseif returnValue[key] ~= memoryValues[key] then
								tester:reportMemory( key, returnValue[key], memoryValues[key], tester:getMemory( key ) ) --
							end
						end
					end ) )

				elseif randomTask == 5 then
					tester:iprint( "Save multi string" )
					local keyValuePair = {}
					for i = 1, math.random( 1000 ) do
						local key = randomString( keyLength )
						local value = randomString( keyLength )
						keyValuePair[key] = value
						tester:setMemory( key, value )
						tester:assert( tableController:save( key, value ) )
					end
				elseif randomTask == 6 then
					tester:iprint( "Save multi table" )

					local testValues = { 1324, 456785677, "hdfiugfd", "df", 0 / 0, -1 / 0, nil, false, true, {}, { {} }, root, resourceRoot, function() end }
					for i = 1, math.random( 100 ) do
						local key = randomString( keyLength )
						local value = {}
						for j = 1, math.random( 1000 ) do value[#value + 1] = testValues[math.random( #testValues )] end
						local memoryData = call( getResourceFromName( "testexport" ), "testExport", cleanTableForJSON( deepcopy( value ) ) )
						tester:assert( type( memoryData ) == "table", "table not converted correctly", memoryData )
						tester:setMemory( key, memoryData )

						tester:assert( tableController:save( key, value ) )
					end
				elseif randomTask == 7 then
					local key = randomString( keyLength )
					local value = randomString( keyLength )
					tester:iprint( "Test duplicate", key )
					tester:setMemory( key, value )
					for i = 1, 5 do tableController:save( key, value ) end
					tableController:save( key, value )

					local request = createTestRequest()
					tableController:query( function( result )
						request.success = true
						tester:assert( #result == 1, "Expected 1 item in the database, got:", #result )
						tester:assert( value == result[1].value, "Expected item in the database to have value:", value, type( value ), ", got:", result[1].value,
						               type( result[1].value ) )
					end, "SELECT value FROM testtable__async WHERE ID = ?", key )

				elseif randomTask == 8 then

					tester:iprint( "Test save > load" )
					tester:stop()

					local keyValuePair = {}
					local keyLength = 3
					for i = 1, 100 do
						local key = randomString( keyLength )
						local value = randomString( keyLength )
						keyValuePair[key] = value
					end

					-- Test cache
					for key, value in pairs( keyValuePair ) do
						tester:setMemory( key, value )
						tester:assert( tableController:save( key, value ) )

						local request = createTestRequest()
						tester:assert( tableController:load( key, function( returnedValue )
							request.success = true
							if returnedValue ~= value then tester:error( "Mismatch cache", returnedValue, type( returnedValue ), value, type( value ) ) end
						end ) )
					end

					-- Restart resource (clearing cache)
					setTimer( function()
						if not restartResource( getResourceFromName( "asyncdatabase" ) ) then tester:error( "Error on restart asyncdatabase" ) end
					end, 100, 1 )

					-- Test storage
					setTimer( function()
						for key, value in pairs( keyValuePair ) do
							local request = createTestRequest()
							tester:assert( tableController:load( key, function( returnedValue )
								request.success = true
								if returnedValue ~= value then tester:error( "Mismatch db data", returnedValue, type( returnedValue ), value, type( value ) ) end
							end ) )
						end
					end, 200, 1 )
					setTimer( function() tester:start() end, 500, 1 )
				elseif randomTask == 9 then
					tester:iprint( "Test requests", getTestRequestCount() )
					checkTestRequests()
				elseif hasRestartResourcePermission and randomTask == 10 and math.random( 3 ) == 1 then
					tester:iprint( "Restart asyncdatabase" )
					if not restartResource( getResourceFromName( "asyncdatabase" ) ) then tester:error( "Error on restart asyncdatabase" ) end
				end
			end
		} )
	end )
	tester:start()
end )

do
	local requests = {} -- Containing db requests
	function createTestRequest()
		local request = { time = getTickCount(), success = false }
		requests[#requests + 1] = request
		return request
	end
	function getTestRequestCount() return #requests end
	function checkTestRequests()
		local timeNow = getTickCount()
		for i = #requests, 1, -1 do
			local request = requests[i]
			if request.success then
				table.remove( requests, i )
			elseif timeNow > request.time + 500 then
				tester:error( "Request is hanging" )
			end
		end
	end
end

function randomString( length )
	local res = ""
	for i = 1, length do res = res .. string.char( math.random( 97, 122 ) ) end
	return res .. "-"
end

-- http://lua-users.org/wiki/CopyTable
function deepcopy( orig )
	local orig_type = type( orig )
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do copy[deepcopy( orig_key )] = deepcopy( orig_value ) end
		setmetatable( copy, deepcopy( getmetatable( orig ) ) )
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end
local validTypes = { number = true, string = true, boolean = true }
function cleanTableForJSON( theTable )
	if type( theTable ) == 'table' then
		for i = 1, #theTable do
			local value = theTable[i]
			local valType = type( value )
			if valType == 'table' then
				cleanTableForJSON( value )
			elseif not validTypes[valType] then -- nil, userdata/pointers
				theTable[i] = false
			end
		end
	end
	return theTable
end
