Tester = {}
local Tester = Tester
function Tester:new()
	return setmetatable( {
		active = false, --
		memory = {}, --
		memoryChanges = {}, --
		jobs = {}
	}, { __index = Tester } )
end

function Tester:start()
	if self.active then return false end
	self.active = true
	local self_ = self
	self.timer = setTimer( function() self_:run() end, 50, 0 )
	return self
end

function Tester:setMemory( key, value )
	self[key] = value
	local memoryChanges = self.memoryChanges
	if not memoryChanges[key] then
		local memoryObject = { key = key, time = getTickCount(), collection = { value }, valueIndex = 2 }
		memoryChanges[key] = memoryObject
	else
		local memoryChange = memoryChanges[key]
		local index = memoryChange.valueIndex
		memoryChange.collection[index] = index
		memoryChange.valueIndex = index + 1
	end
	return self
end

function Tester:iprint( ... )
	iprint( "[TESTER]", ... )
	return self
end

function Tester:getMemory( key ) return self[key] end

function Tester:stop()
	if not self.active then return false end
	self.active = false
	if isTimer( self.timer ) then killTimer( self.timer ) end
	self.timer = nil
	return self
end

function Tester:addJob( job )
	if type( job ) ~= "table" then error( "Expected table", 2 ) end
	if type( job.func ) ~= "function" then error( "Expected function on index 'func'", 2 ) end
	local jobs = self.jobs
	jobs[#jobs + 1] = job
	return self
end

function Tester:addPreCall( func )
	if type( func ) ~= "function" then error( "Expected function at argument 1", 2 ) end
	self.preCallFunc = func
	return self
end

function Tester:run()
	if not self.active then return end

	if self.preCallFunc then self.preCallFunc( self ) end

	local self_ = self
	local job = self.jobs[1]
	table.remove( self.jobs, 1 )
	if type( job ) == "table" and type( job.func ) == "function" then
		local callState = xpcall( function() job.func( self_ ) end, function( err ) outputDebugString( "ERROR(Traceback): " .. err, 4, 255, 0, 0 ) end )
	end
end

function Tester:error( ... )
	self:stop()
	error( "[TESTER][ERROR] " .. inspect( { ... } ), 2 )
end

function Tester:assert( status, ... ) if not status then self:error( "Assert: " .. inspect( { ... } ), 2 ) end end

function Tester:reportMemory( key, ... )
	local memoryChanges = self.memoryChanges

	local inspectResult = {}
	local inspectValues = { ... }
	for i = 1, #inspectValues do
		inspectResult[#inspectResult + 1] = { index = i, val = inspectValues[i], type = type( inspectValues[i] ), length = #tostring( inspectValues[i] ) }
	end

	if memoryChanges[key] then

		self:error( "Memory incorrect " .. inspect( memoryChanges[key] ) .. " | " .. inspect( inspectResult, { depth = 4 } ), 2 )
	else
		self:error( "Can't find memory change on key " .. tostring( key ) .. " | " .. inspect( inspectResult, { depth = 4 } ), 2 )
	end
end
