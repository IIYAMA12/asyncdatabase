--[[
    Load the extension file
]] --
local FILE_PATH = "extensions/extension_s.lua"
local extensionCode
if fileExists( FILE_PATH ) then
	local resourceName = getResourceName( getThisResource() )
	local extensionFile = fileOpen( FILE_PATH, true )

	if extensionFile then
		extensionCode = 'local storageResource = getResourceFromName("' .. resourceName .. '")\n'
		extensionCode = extensionCode .. fileRead( extensionFile, fileGetSize( extensionFile ) )
		fileClose( extensionFile )
	end
end

function getExtension()
	if not extensionCode then error( "Extension code failed to load from file." ) end
	return extensionCode
end
