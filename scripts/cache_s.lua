--[[
    This code will cache database values so that you can have instant results.
	dic = dictionary {["string"] = value}
]] --
-----
local cacheCleanInterval = 600000 -- DEFAULT 600000

--[[
 * <CacheItem>
 * { 
	value = strings / tables (converted through JSON) / numbers / booleans / nil, 
	time = number, 
	key = string 
   }
]] --
-----
-----
local CacheManager = { dic = {}, list = {} }
function CacheManager:new( tableName )
	local cacheManager = { dic = {}, list = {}, watchList = {}, thrashCount = 0 }
	self.dic[tableName] = cacheManager
	self.list[#self.list + 1] = cacheManager
	return cacheManager
end

--[[
 * tableName = string
 > return cacheManager
]]
function CacheManager:init( tableName ) return self.dic[tableName] or self:new( tableName ) end

local tableRemove = table.remove
local removeOldCacheItems

--[[
 * tableName = string
 * key = string / number (converted to string)
 * value = strings / tables (converted through JSON) / numbers / booleans / nil
 > return <CacheItem>
]]
function addToCache( tableName, key, value )
	local cacheManager = CacheManager:init( tableName )

	removeFromCache( tableName, key )

	local cacheItem = { value = value, time = getTickCount(), key = key }
	cacheManager.dic[key] = cacheItem

	local cacheList = cacheManager.list
	cacheList[#cacheList + 1] = cacheItem

	if cacheManager.thrashCount > 1000 then removeOldCacheItems() end

	return cacheItem
end

--[[
 * tableName = string
 * key = string / number (converted to string)
 > return strings / tables (converted through JSON) / numbers / booleans / nil
]]
function getFromCache( tableName, key ) return CacheManager:init( tableName ).dic[key] end

--[[
 * tableName = string
 * key = string / number (converted to string)
 > return successStatus boolean
]]
function removeFromCache( tableName, key )
	checkWatchItems( tableName, key )
	local cacheManager = CacheManager:init( tableName )
	local cacheDic = cacheManager.dic
	local cacheItem = cacheDic[key]
	if cacheItem then
		cacheManager.thrashCount = cacheManager.thrashCount + 1
		cacheItem.dead = true
		cacheItem.value = nil
		cacheDic[key] = nil
		return true
	end
	return false
end

function removeOldCacheItems()
	local cacheManagerList = CacheManager.list
	for managerIndex = #cacheManagerList, 1, -1 do
		local cacheManager = cacheManagerList[managerIndex]

		local cacheList = cacheManager.list
		local cacheDic = cacheManager.dic
		if CONFIG.debugMode then iprint( "removeOldCacheItems START #cacheList=", #cacheList ) end
		local timeNow = getTickCount()
		for i = #cacheList, 1, -1 do
			local cacheItem = cacheList[i]
			if cacheItem.dead or timeNow > cacheItem.time + cacheCleanInterval then
				cacheDic[cacheItem.key] = nil
				tableRemove( cacheList, i )
			end
		end
		if CONFIG.debugMode then iprint( "removeOldCacheItems END #cacheList=", #cacheList ) end
		cacheManager.thrashCount = 0
	end
end

--[[

	Make it possible to watch cache changes

]]
local cacheWatchList = {}
CacheWatcher = {}
function CacheWatcher:new() return setmetatable( { active = false }, { __index = CacheWatcher } ) end

function CacheWatcher:watch( tableName, key, func )
	self.active = true
	self.tableName = tableName
	self.key = key
	self.func = func
	cacheWatchList[#cacheWatchList + 1] = self
	return self
end

function CacheWatcher:unwatch()
	if self.active then
		self.active = false
		for i = #cacheWatchList, 1, -1 do
			if cacheWatchList[i] == self then
				tableRemove( cacheWatchList, i )
				break
			end
		end
	end
	return self
end

--[[
 * tableName = string
 * key = string / number (converted to string)
 > return true / nil
]]
function checkWatchItems( tableName, key )
	for i = #cacheWatchList, 1, -1 do
		local watchItem = cacheWatchList[i]
		if watchItem.key == key and watchItem.tableName == tableName then watchItem.func() end
	end
	return true
end

--[[

	Init

]]
function initCache() setTimer( removeOldCacheItems, cacheCleanInterval, 0 ) end
