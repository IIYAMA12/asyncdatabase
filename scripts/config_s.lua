CONFIG = {
	-- Mode: SQLite / MySQL
	sqlite = true, -- < true = SQLite, false = MySQL
	------------------------------------------
	------------------------------------------
	-- SQLite ONLY
	sqliteFilePath = "database.db", --
	------------------------------------------
	-- MySQL ONLY
	username = "", --
	password = "", --
	dbname = "storage", --
	host = "localhost", --
	port = nil, --
	------------------------------------------
	-- DEBUG --
	debugMode = false
}

--[[
	Load .env file
]]
(function()
	local content = getEnvFileContent()
	if not content then return end
	local env = parseEnv( content )
	if not env then return end
	for key, value in pairs( env ) do
		if value == "true" or value == "false" then
			CONFIG[key] = value == "true"
		elseif value ~= "" then
			CONFIG[key] = value
		end
	end
end)()

