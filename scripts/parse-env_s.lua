--[[
    Inspiration from:
    https://github.com/motdotla/dotenv/blob/master/lib/main.js
    Repository owner: https://github.com/motdotla
]] -- 
function getEnvFileContent()
	if not fileExists( ".env" ) then return end
	local file = fileOpen( ".env", true )
	if not file then return end
	local content = fileRead( file, fileGetSize( file ) )
	fileClose( file )
	return content
end

local function trim( s ) return s:match '^%s*(.*%S)' or '' end

function parseEnv( src )
	local env = {}

	-- Convert buffer to string
	local str = tostring( src )

	-- Convert line breaks to same format
	str = str:gsub( "\r\n?", "\n" )

	local lines = split( str, "\n" )
	for i = 1, #lines do
		local line = lines[i]
		if line ~= "" then

			local key = line:match( "^%s*(.-)%s*=" )

			-- Default nil to empty string
			local value = line:match( "=(.-)$" ) or ""

			-- Remove whitespace
			value = trim( value )

			-- Check if double quoted
			local maybeQuote = value:sub( 1, 1 )

			-- Remove surrounding quotes
			value = value:gsub( "^(['\"])(.-)%1$", "%2" )

			-- Expand newlines if double quoted
			if maybeQuote == '"' then
				value = value:gsub( "\\n", "\n" )
				value = value:gsub( "\\r", "\r" )
			end

			-- Add to env
			if key then env[key] = value end
		end
	end

	return env
end
