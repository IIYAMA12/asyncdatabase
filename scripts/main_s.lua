local connection
local keyTypeConstrain = { ["number"] = true, ["string"] = true }
local callbackIDGenerator = Identifier:newGenerator()
local tableInitSet = {}

local sqlite = CONFIG.issqlite -- true = sqlite, false = mysql

local stringFormat = string.format
--[[
	For security reasons the queries are pre-build here.
]]
local queries = {
	shared = {
		select = "SELECT * FROM %s__async WHERE `ID` = ?", --
		selectMultipleStart = "SELECT * FROM %s__async WHERE `ID` IN ( ?", --
		selectMultipleEnd = " )", --
		selectMultipleAdditional = ", ? ", --
		selectLimit = "SELECT * FROM %s__async WHERE `ID` = ? LIMIT 1", --
		delete = "DELETE FROM %s__async WHERE `ID` = ?" --
	}, sqlite = {
		update = "UPDATE %s__async SET `value` = ?, `valueType` = ? WHERE ID = ?", -- 
		insert = "INSERT INTO %s__async (`ID`, `value`, `valueType`) VALUES(?, ?, ?)", -- 
		insertUpdate = "INSERT OR REPLACE INTO %s__async (`ID`, `value`, valueType) VALUES ( ?, ?, ? )"
	}, mysql = {
		insertUpdate = "INSERT INTO %s__async (`ID`, `value`, `valueType`) VALUES ( ?, ?, ? ) ON DUPLICATE KEY UPDATE `ID`=VALUES(`ID`), `value`=VALUES(`value`), `valueType`=VALUES(`valueType`)"
	}
}

addEventHandler( "onResourceStart", resourceRoot, function()
	initCache()
	-- 
	if sqlite then
		if CONFIG.sqliteFilePath == "" then
			outputDebugString( "Prop 'sqliteFilePath' in .env has incorrect value. Stopping resource.", 2 )
			cancelEvent()
			return
		end
		connection = dbConnect( "sqlite", CONFIG.sqliteFilePath, nil, nil, "share=1" )
	else
		connection = dbConnect( "mysql", "dbname=" .. CONFIG.dbname .. ";host=" .. CONFIG.host .. ";charset=utf8" ..
						                        (CONFIG.port and (";port=" .. CONFIG.port) or ""), CONFIG.username, CONFIG.password )
	end
	CONFIG.username, CONFIG.password = nil, nil

	if not connection then
		outputDebugString( "Unable to connect to database. Stopping resource.", 2 )
		cancelEvent()
		return
	else
		outputDebugString( "Successfully connected to database!" )
	end

	if sqlite then
		local qh = dbQuery( connection, [[SELECT name FROM sqlite_master WHERE type='table' AND name LIKE '%__async']] )

		local result = dbPoll( qh, -1 )
		if result then for _, theTable in pairs( result ) do tableInitSet[theTable.name] = true end end
	else
		local qh = dbQuery( connection, stringFormat( [[SELECT table_name as name FROM information_schema.tables
		WHERE table_schema = '%s' AND table_name REGEXP '__async$' ]], CONFIG.dbname, CONFIG.dbname ) )
		local result = dbPoll( qh, -1 )
		if result then for _, theTable in pairs( result ) do tableInitSet[theTable.name] = true end end
	end
end, false )

local function testTable( tableName )
	tableName = tableName .. "__async"
	if tableInitSet[tableName] then return true end
	return false, "Table is not init " .. tableName
end

--[[
	* tableName = string
	> return status boolean [, message string ]
]]
function tableInit( tableName )
	if type( tableName ) ~= "string" then return false, "Table is not a string" end
	if #tableName > 20 then return false, "Table names shouldn't be longer than 20 character for security reasons." end
	tableName = tableName .. "__async"
	if tableInitSet[tableName] then return true, "Note: Table is already init" end

	tableInitSet[tableName] = true

	if sqlite then
		dbExec( connection, stringFormat( [[
			CREATE TABLE IF NOT EXISTS %s 
			( 
				ID VARCHAR (50) NOT NULL, 
				value TEXT, 
				valueType TEXT, 
				PRIMARY KEY (ID), 
				
				CONSTRAINT CHK_%s CHECK (valueType IN  ('number', 'table', 'boolean', 'nil', 'string'))
			)
			]], tableName, tableName ) )
		dbExec( connection, stringFormat( "CREATE UNIQUE INDEX IF NOT EXISTS ? ON %s(ID)", tableName ), stringFormat( "%s_ID", tableName ) )
	else
		dbExec( connection, stringFormat( [[
			CREATE TABLE IF NOT EXISTS %s
			( 
				ID VARCHAR (50) NOT NULL, 
				value TEXT, 
				valueType TEXT, 
				PRIMARY KEY (ID)
			)
			]], tableName ) )
	end

	return true
end

--[[
	* tableName = string
	* key = string / number (converted to string)
	* value = strings / tables (converted through JSON) / numbers / booleans / nil
	> return status boolean [, message string ]
]]
function saveData( tableName, key, value )
	local testStatus, testResponse = testTable( tableName )
	if not testStatus then return testStatus, testResponse end
	--[[
		Key validation
	]]
	local keyType = type( key )
	if not keyTypeConstrain[keyType] then
		outputDebugString( "Unable to save data with key: " .. tostring( key ) .. ", type: " .. keyType .. ", expected types: " ..
						                   inspect( keyTypeConstrain ), 2 )
		return false
	end

	if keyType == "number" then key = tostring( key ) end

	if key:len() > 50 then
		outputDebugString( "Unable to save data with keys that have more than 50 characters.", 2 )
		return false
	end

	--[[
		Value validation and prepare for db
	]]
	local readyValue, valueType = toDatabaseFormat( value )
	if not readyValue then
		outputDebugString( "Unable to save value. Incorrect value type.", 2 )
		return false
	end

	-- normalize table cache value (so that there is no mismatch with the one in the database)
	if valueType == "table" then value = fromJSON( readyValue ) end

	local cacheItem = getFromCache( tableName, key )
	if cacheItem and cacheItem.value == value then return true end

	addToCache( tableName, key, value )

	--[[
			Save to database
		]]
	if sqlite then
		dbExec( connection, stringFormat( queries.sqlite.insertUpdate, tableName ), key, readyValue, valueType )
	else
		dbExec( connection, stringFormat( queries.mysql.insertUpdate, tableName ), key, readyValue, valueType )
	end
	return true
end

--[[
	* tableName = string
	* key = string / number (converted to string) OR { string / number, ... }
	> return status boolean [, message string / response {type = "", value = ""} ]
]]
function loadData( tableName, key )
	local testStatus, testResponse = testTable( tableName )
	if not testStatus then return testStatus, { type = "ERROR", value = testResponse } end

	local theSourceResource = sourceResource
	local callbackID = callbackIDGenerator:generate()
	local abortCallBack = false

	local callState, callResponse = xpcall( function()

		local keyType = type( key )
		if keyType ~= "table" then --[[
				Single value
			]]
			if not keyTypeConstrain[keyType] then
				local msg = "Unable to load data with key: " .. tostring( key ) .. ", type: " .. keyType .. ", expected types: " .. inspect( keyTypeConstrain )
				outputDebugString( msg, 2 )
				return { type = "ERROR", value = msg }
			end

			if keyType == "number" then key = tostring( key ) end

			--[[
				Try from cache
			]]
			local cacheItem = getFromCache( tableName, key )
			if cacheItem then return { type = "CACHE", value = cacheItem.value } end

			local cacheResult = true
			local watcher = CacheWatcher:new()
			watcher:watch( tableName, key, function()
				watcher:unwatch()
				cacheResult = false
			end )

			--[[
				Get from database
			]]
			local callback = function( qh )
				watcher:unwatch()
				local result = dbPoll( qh, 0 )
				local readyValue
				if result and #result > 0 then
					local databaseResult = result[1]
					readyValue = fromDatabaseFormat( databaseResult.value, databaseResult.valueType )
				end

				if cacheResult then addToCache( tableName, key, readyValue ) end
				if not abortCallBack then call( theSourceResource, "responseStorageData", callbackID, readyValue ) end
			end
			dbQuery( callback, connection, stringFormat( queries.shared.selectLimit, tableName ), key )
			return { type = "CALLBACK", value = callbackID }
		else --[[
				Multiple values
			]]
			local readyValues = {}

			local keyCollection = key

			--[[
				Try from cache
			]]
			local cacheResults = {}
			local watcherCollection = {}
			for i = #keyCollection, 1, -1 do
				local key = tostring( keyCollection[i] )

				local cacheItem = getFromCache( tableName, key )
				if cacheItem then
					readyValues[key] = cacheItem.value
					table.remove( keyCollection, i )
				else
					cacheResults[key] = true
					keyCollection[i] = key

					local watcher = CacheWatcher:new()
					watcherCollection[#watcherCollection + 1] = watcher
					watcher:watch( tableName, key, function()
						watcher:unwatch()
						cacheResults[key] = false
					end )
				end
			end

			--[[
				Try from database
			]]
			local callback = function( qh )
				for i = 1, #watcherCollection do watcherCollection[i]:unwatch() end

				local result = dbPoll( qh, 0 )
				if result and #result > 0 then
					for i = 1, #keyCollection do
						local key = keyCollection[i]
						local resultIndex = tableFind( result, "ID", key )
						if resultIndex then
							local value = fromDatabaseFormat( result[resultIndex].value, result[resultIndex].valueType )
							if cacheResults[key] then addToCache( tableName, key, value ) end
							readyValues[key] = value
						end
					end
				else
					--[[
						No results, tell the cache that there is nothing in the database
					]]
					for i = 1, #keyCollection do
						local key = keyCollection[i]
						if cacheResults[key] then addToCache( tableName, key, nil ) end
					end
				end
				if not abortCallBack then call( theSourceResource, "responseStorageData", callbackID, readyValues ) end
			end

			if #keyCollection > 0 then
				dbQuery( callback, connection,
				         stringFormat( queries.shared.selectMultipleStart, tableName ) ..
								         string.rep( queries.shared.selectMultipleAdditional, #keyCollection - 1 ) .. queries.shared.selectMultipleEnd,
				         unpack( keyCollection ) )
				return { type = "CALLBACK", value = callbackID }
			else
				return { type = "CACHE", value = readyValues }
			end
		end
	end, function( err ) outputDebugString( "ERROR(Traceback): " .. err, 4, 255, 0, 0 ) end )

	if not callState then
		abortCallBack = true
		return false, { type = "ERROR", value = "See traceback error." }
	end
	return true, callResponse
end

--[[
	* tableName = string
	* key = string / number (converted to string)
	> return status boolean [, message string ]
]]
function removeData( tableName, key )
	local testStatus, testResponse = testTable( tableName )
	if not testStatus then return testStatus, testResponse end
	--[[
		Key validation
	]]
	local keyType = type( key )
	if not keyTypeConstrain[keyType] then
		outputDebugString( "Unable to remove data with key: " .. tostring( key ) .. ", type: " .. keyType .. ", expected types: " ..
						                   inspect( keyTypeConstrain ), 2 )
		return false
	end

	if keyType == "number" then key = tostring( key ) end

	if key:len() > 50 then
		outputDebugString( "Unable to remove data with keys that have more than 50 characters.", 2 )
		return false
	end

	removeFromCache( tableName, key )
	return dbExec( connection, stringFormat( queries.shared.delete, tableName ), key )
end

do
	--[[

		Convert data from MTA to database

	]]
	local toDatabaseMethods = {
		["table"] = function( value ) return toJSON( value, true ) end, --
		["number"] = function( value ) return tostring( value ) end, --
		["boolean"] = function( value ) return tostring( value ) end, --
		["nil"] = function( value ) return "" end -- 
	}
	function toDatabaseFormat( value )
		local valueType = type( value )
		local readyValue
		if valueType == "string" then
			readyValue = value
		elseif toDatabaseMethods[valueType] then
			readyValue = toDatabaseMethods[valueType]( value )
		end
		return readyValue, valueType
	end

	--[[

		Convert data from database to MTA

	]]
	local fromDatabaseMethods = {
		["table"] = function( value ) return fromJSON( value ) end, --
		["number"] = function( value ) return tonumber( value ) end, --
		["boolean"] = function( value ) return value == "true" end, --
		["nil"] = function( value ) return end -- 
	}
	function fromDatabaseFormat( value, valueType )
		local readyValue
		if valueType == "string" then
			readyValue = value
		elseif fromDatabaseMethods[valueType] then
			readyValue = fromDatabaseMethods[valueType]( value )
		end
		return readyValue
	end
end

--[[
	* callbackAvailable = boolean
	* query = string
	* ... arguments
	* ...
	* ...
	> return {type = "", value = ""}
]]
function runQuery( callbackAvailable, query, ... )

	local arguments = { ... }
	local theSourceResource = sourceResource

	local callbackID
	if callbackAvailable then callbackID = callbackIDGenerator:generate() end
	local callState = xpcall( function()
		if callbackID then
			local callback = function( qh )
				local result = dbPoll( qh, 0 )
				call( theSourceResource, "responseStorageData", callbackID, result )
			end
			dbQuery( callback, connection, query, unpack( arguments ) )
		else
			dbExec( connection, query, unpack( arguments ) )
		end
	end, function( err ) outputDebugString( "ERROR(Traceback): " .. err, 4, 255, 0, 0 ) end )

	if not callState then return { type = "ERROR", value = "See traceback error." } end
	if callbackID then return { type = "CALLBACK", value = callbackID } end
	return { type = "NONE" }
end
