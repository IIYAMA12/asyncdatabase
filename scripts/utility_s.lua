do
	Identifier = {}
	function Identifier:newGenerator() return setmetatable( { index = 0 }, { __index = Identifier } ) end
	function Identifier:generate()
		local index = self.index + 1
		self.index = index
		return "id:" .. getTickCount() .. "|" .. index
	end
end

function tableFind( theTable, key, value ) for i = 1, #theTable do if theTable[i][key] == value then return i end end end
